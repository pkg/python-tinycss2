python-tinycss2 (1.2.1-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 23:47:58 +0000

python-tinycss2 (1.2.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-tinycss2-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 13:58:43 +0000

python-tinycss2 (1.2.1-1) unstable; urgency=low

  * New upstream release.
  * Drop pytest plugin patch, no longer used by upstream.
  * Refresh patches.
  * Install testfiles using d/pybuild.testfiles.
  * Update d/copyright with new years.
  * Bump Standards-Version to 4.6.1.0.

 -- Michael Fladischer <fladi@debian.org>  Wed, 19 Oct 2022 15:21:06 +0000

python-tinycss2 (1.1.1-2) unstable; urgency=medium

  * Team upload.
  * dh-python-pep517 is replaced by pybuild-plugin-pyproject.

 -- Stefano Rivera <stefanor@debian.org>  Tue, 08 Feb 2022 22:28:20 -0400

python-tinycss2 (1.1.1-1) unstable; urgency=low

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-tinycss2-common: Add Multi-Arch: foreign.

  [ Michael Fladischer ]
  * New upstream release.
  * Use dh-python-pep517 to support pyproject.toml build.
  * Drop 0002-Restore-missing-docs-dir-from-github.patch, docs are
    included in upstream tarball now.
  * Refresh patches.
  * Bump debhelper version to 13.
  * Bump Standards-Version to 4.6.0.1.
  * Set Rules-Requires-Root: no.
  * Add flit to Build-Depends, required by build system.
  * Add python3-pytest and python3-pytest-flake8 to Build-Depends,
    required by tests.
  * Update year in d/copyright.
  * Upstream moved css-parsing-tests location.
  * Clean up test artifacts in PYBUILD_AFTER_TEST.
  * Use uscan version 4.
  * Enable upstream testsuite for autopkgtests.
  * Add patch to prevent privacy breach through remote CSS files.

 -- Michael Fladischer <fladi@debian.org>  Wed, 22 Dec 2021 22:59:05 +0000

python-tinycss2 (1.0.2-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:16:53 +0200

python-tinycss2 (1.0.2-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.

  [ Scott Kitterman ]
  * Add d/gbp.conf so debian/master is used as the default branch
  * New upstream release
  * Update d/p/0001-Do-not-use-pytest-isort-and-pytest-flake8-in-tests-n.patch
    to match upstream changes
  * Drop d/p/0002-Decode-test-data-using-UTF-8.patch, incorporated upstream
  * Drop d/p/0003-Convert-travis-ci-badge-to-text-link-to-avoid-privac.patch,
    related code removed upstream
  * Readd docs dir upstream failed to include in the release tarball

 -- Scott Kitterman <scott@kitterman.com>  Fri, 09 Aug 2019 16:06:20 -0400

python-tinycss2 (0.6.1-1) unstable; urgency=low

  * Initial release (Closes: #903817).

 -- Michael Fladischer <fladi@debian.org>  Sun, 15 Jul 2018 12:08:24 +0200
